select * from link.link_cls_corporation where corporate_id in (104040000000, 105070000000, 107790000000, 107800000000);

select * from link.link_cls_corporation where upper(corporate_name_english_1) like '%PARK%';

select * from link.view_facttrans_plus where corporation_id=107790000000;
select * from link.view_facttrans_plus where branch_no=1077952730;

-- Points Exchange analysis incl Starhub
drop table if exists #pointsexchange;
select a.csn, min(transaction_date) as exchange_date, sum(point_earned) as exchanged_LP, pointid, 
sum(transcount) as exchange_count, corporation_id, corporate_name_english_1 as partner
into #pointsexchange
from link.view_facttrans_plus a
join link.link_cls_corporation b on a.corporation_id=b.corporate_id
where corporation_id in (104040000000, 105070000000, 107790000000, 107800000000)
and transaction_date>='2019-02-27'
group by a.csn, pointid, corporation_id, partner;

drop table if exists #redemptions;
select partner, a.csn, exchange_date, exchange_count, exchanged_LP, case when sum(point_earned)<exchanged_LP then sum(point_earned) else exchanged_LP end as redeemed_LP, sum(est_gross_transaction_amt) as GMV, b.corporation_id, corporate_name_english_1, redeemed_LP/150.00 as LP_value, sum(transcount) as transactions, b.pointid 
into #redemptions
from #pointsexchange a 
join link.view_facttrans_plus b on a.csn=b.csn
join link.link_cls_corporation c on b.corporation_id=c.corporate_id
where transaction_date between exchange_date and dateadd(day, 30, exchange_date) and b.corporation_id not in (104040000000, 105070000000, 107790000000, 107800000000) and a.pointid='I' and b.pointid='R'
group by partner, a.csn, exchange_date, exchange_count, exchanged_LP, b.corporation_id, corporate_name_english_1, b.pointid;

select case when corporate_name_english_1 in ('NTUC FAIRPRICE', 'CHEVRON SINGAPORE PTE LTD') then corporate_name_english_1 else 'others' end as merchant, sum(redeemed_LP), count(distinct csn) from #redemptions
group by merchant;

select sum(point_earned), corporate_name_english_1 from link.view_facttrans_plus a
join link.link_cls_corporation b on a.corporation_id=b.corporate_id
where transaction_date>='2019-02-27'
and pointid='R'
group by corporate_name_english_1;

drop table if exists #redeemed;
select distinct csn, exchange_date, corporate_name_english_1, corporation_id into #redeemed from #redemptions;

drop table if exists #gmv;
select a.csn, a.corporate_name_english_1, a.corporation_id, sum(b.est_gross_transaction_amt) as gmv
into #gmv from #redeemed a
join link.view_facttrans_plus b on a.csn=b.csn and a.corporation_id=b.corporation_id
where transaction_date between exchange_date and dateadd(day, 30, exchange_date)
group by a.csn, a.corporate_name_english_1, a.corporation_id;

select case when corporate_name_english_1 in ('NTUC FAIRPRICE', 'CHEVRON SINGAPORE PTE LTD') then corporate_name_english_1 else 'others' end as merchant, sum(gmv) from #gmv
group by merchant;

-- pre vs post
drop table if exists #issued;
select csn, exchange_date
into #issued
from #pointsexchange
where pointid='R';

select * from #issued;

select a.csn, exchange_date, sum(case when transaction_date >= dateadd(day, -14, exchange_date) and transaction_date < exchange_date then est_gross_transaction_amt else 0 end) as pre, 
sum(case when transaction_date >= exchange_date and transaction_date < dateadd(day, 14, exchange_date) then est_gross_transaction_amt else 0 end) as post, post-pre as difference
from #issued a
join link.view_facttrans_plus b on a.csn=b.csn
where pointid='I' and corporation_id='FP' and exchange_date < dateadd(day, 14, exchange_date)
group by a.csn, exchange_date;

-- Find new acquisitions for FP
select a.csn, b.pointid, min(transaction_date) from #pointsexchange a
join link.view_facttrans_plus b on a.csn=b.csn
where b.corporation_id in ('FP', 101770000000) and a.pointid='I' 
and yearid in ('2017','2018','2019')
group by a.csn, b.pointid;

-- Excl Starhub
drop table if exists #pointsexchange;
select a.csn, min(transaction_date) as exchange_date, sum(point_earned) as exchanged_LP, pointid, 
sum(transcount) as exchange_count, corporation_id, corporate_name_english_1 as partner
into #pointsexchange
from link.view_facttrans_plus a
join link.link_cls_corporation b on a.corporation_id=b.corporate_id
where corporation_id in (105070000000, 107790000000, 107800000000)
and transaction_date>='2019-02-27'
group by a.csn, pointid, corporation_id, partner;

drop table if exists #redemptions;
select partner, a.csn, exchange_date, exchange_count, exchanged_LP, case when sum(point_earned)<exchanged_LP then sum(point_earned) else exchanged_LP end as redeemed_LP, sum(est_gross_transaction_amt) as GMV, b.corporation_id, corporate_name_english_1, redeemed_LP/150.00 as LP_value, sum(transcount) as transactions, b.pointid 
into #redemptions
from #pointsexchange a 
join link.view_facttrans_plus b on a.csn=b.csn
join link.link_cls_corporation c on b.corporation_id=c.corporate_id
where transaction_date between exchange_date and dateadd(day, 30, exchange_date) and b.corporation_id not in (104040000000, 105070000000, 107790000000, 107800000000) and a.pointid='I' and b.pointid='R'
group by partner, a.csn, exchange_date, exchange_count, exchanged_LP, b.corporation_id, corporate_name_english_1, b.pointid;

select case when corporate_name_english_1 in ('NTUC FAIRPRICE', 'CHEVRON SINGAPORE PTE LTD') then corporate_name_english_1 else 'others' end as merchant, sum(redeemed_LP), sum(redeemed_LP)/150 as LPvalue, count(distinct csn) from #redemptions
group by merchant;

select sum(point_earned), corporate_name_english_1 from link.view_facttrans_plus a
join link.link_cls_corporation b on a.corporation_id=b.corporate_id
where transaction_date>='2019-02-27'
and pointid='R'
group by corporate_name_english_1;

drop table if exists #redeemed;
select distinct csn, exchange_date, corporate_name_english_1, corporation_id into #redeemed from #redemptions;

drop table if exists #gmv;
select a.csn, a.corporate_name_english_1, a.corporation_id, sum(b.est_gross_transaction_amt) as gmv
into #gmv from #redeemed a
join link.view_facttrans_plus b on a.csn=b.csn and a.corporation_id=b.corporation_id
where transaction_date between exchange_date and dateadd(day, 30, exchange_date)
group by a.csn, a.corporate_name_english_1, a.corporation_id;

select case when corporate_name_english_1 in ('NTUC FAIRPRICE', 'CHEVRON SINGAPORE PTE LTD') then corporate_name_english_1 else 'others' end as merchant, sum(gmv) from #gmv
group by merchant;
